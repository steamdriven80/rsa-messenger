rsa-messenger
=============

RSA Implementation in Javascript

License: 
Use it however you like, I don't need or ask for any credit.  Some of this code was summarized from various places
around the internet, most notably the Successive Squaring function.  Unfortunately, I no longer remember where I saw a
similar implementation, however, know the implementation is mine, the idea is not.

Description:
I wrote this code to showcase the topics covered in my Intro to Number Theory class at Santa Fe College, Gainesville, FL.
While still severely lacking in any kind of real-world security sense, it does easily emphasize the process RSA uses to
encrypt and decrypt data.  A Number Theory book would be well used in understanding the theory though, if that's your intention.

Todo:
In addition to my javascript implementation, we were calculating these encrypted values using paper and pencil (and calculator)
so I only needed several digit keys maximum.  Therefore, my implementation is limited to 32-bit pq, or 16-bit p and 16-bit q, which are
not very big primes!  Therefore, I need to:

* Implement code routines for arbitrary n-bit operations (multiply, divide, add, subtract)
* Change P, Q, PQ, and K values to use these n-bit variables
* Research using a key to sign data (which is unfortunately not a topic we covered in class)
* Implement the industry standard base64 public key-modulus pair as a single string (allowing easy copy+paste sharing, etc.)

Thanks for looking at my project, again take whatever you can get a use out of, just let me know so I can improve this project
for others!

Chris Pergrossi
