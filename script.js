
// successive square function that raises base ^ power (mod modulo)
function sSquare( base, power, modulo )
{
    ///////////////////////////////////
    // binary expansion of the power
    //
    // we're going to discover the largest power of 2 we'll need
    // right now only handles up to 32-bit powers
    var maxBinary = 0;
    var flag;

    for(var i=0; i<32; i++)
    {
        flag = 1<<i;
        if( power&flag )
        {
            maxBinary = i;
        }
    }

    var powerList = [], value = base;

    powerList.push(value);  // push base ^ 1

    // build the successive squaring list for powers of 2 up to our max
    for(i=1; i<=maxBinary; i++) {
        // square the values (mod modulo)
        value = (value * value) % modulo;

        powerList.push(value);
    }

    // iterate through our binary expansion (we're cheating and using the fact that #'s are stored in binary form)
    // and multiply together the powers of two, mod modulo each time
    value = 1;

    for(i=0; i<32; i++) {
        flag = 1<<i;
        if( power&flag )
        {
            value = (value * powerList[i]) % modulo;
        }
    }

    // value is the final successively squared result
    return value;
}

// finds the gcd
function gcd(a,b)
{
    if (b == 0)
    {return a}
    else
    {return gcd(b, a % b)}
}

// extended euclidean algorithm
function xeuclid(a,b)
{
    if (b == 0)
    {return [1, 0, a];}
    else
    {
        temp = xeuclid(b, a % b);
        x = temp[0];
        y = temp[1];
        d = temp[2];
        return [y, x-y*Math.floor(a/b), d];
    }
}

// finds the kth root of the equation X ^ k = b (mod pq)
function kRoot( k, b, p, q ) {
    if( gcd(p, q) != 1 )
    {
        alert("simple test: p & q are not relatively prime");
        return;
    }

    // this function does not test if p and q are absolutely prime, it will give wonky values if they're not

    // use a negative phi value for extended euclidean
    var phi = (p-1) * (q-1);
    var solution = xeuclid( k, phi );

    // if equation solution are not positive numbers, make them so
    if( solution[0] < 0 || solution[1] < 0 )
    {
        solution[0] += phi;
        solution[1] += k;
    }

    var value = sSquare(b, solution[0], p*q);

    return value;
}

// this function converts ASCII codes into the unique encoding scheme we use for our project
function encode(asciiValue) {
    asciiValue = asciiValue.charCodeAt(0);

    // case insensitive move all alphabet characters to 0-based values
    if( asciiValue >= 97 && asciiValue <= 122 ) { asciiValue -= 97; }
    if( asciiValue >= 65 && asciiValue <= 90 ) { asciiValue -= 65; }

    // check if this is a crazy unknown character
    if( asciiValue > 26 ) { asciiValue = asciiValue%26; }

        // add the alphabet shift we're using in our project
        asciiValue += 11;


    return asciiValue;
}

// this function converts our unique code into ASCII codes for display
function decode(codeValue) {

        // remove the alphabet shift we're using
        codeValue -= 11;

    // check if our character is out of bounds
    if( codeValue > 26 ) { codeValue = codeValue%26; }

    // use capital letters they're easier to read
    codeValue += 65;

    return String.fromCharCode(codeValue);
}

// this will encrypt a message with the public key pair: pq and k
function encrypt( message, pq, k ) {

    // this will remove all spaces from the message
    message = message.replace(/\W/g, '');

    // if there's an odd number of characters, add a null character
    if( message.length%2 != 0 ) { message = message + 'A'; length+=1; }

    var blocks = message.match(/.{1,2}/g);        // this will split the string into two character (or one at the end) blocks
    var value = 0;

    // this is used to store the message.  We don't use it in this implementation, but
    // it could be useful later!
    var cipherText = [];

    // this is used to print the cipher text to the output panel
    var output_msg = '';

    for(var i=0; i<blocks.length; i++) {

        // convert two characters at a time into a 4 digit number in our specific encoding scheme
        value = encode(blocks[i][0]) * 100 + encode(blocks[i][1]);

        // successive square the characters using the public key and power values
        value = sSquare(value, k, pq);

        cipherText.push(value);

        output_msg += value + " ";
    }

    $("#msg_output").html( output_msg );
}

// this will decrypt a space-delimited message into blocks of two characters
// we must pass the p and q values (the secret key) in order to compute phi
function decrypt( message, p, q, k ) {
    var blocks = message.split(" ");        // this will split the space-seperated string into blocks of encrypted 2-letters
    var value = 0;

    // this is used to store the message.  We don't use it in this implementation, but
    // it could be useful later!
    var plainText = [];

    // this is used to print the cipher text to the output panel
    var output_msg = '';
    var charA, charB;

    for(var i=0; i<blocks.length; i++) {

        blocks[i] = parseInt(blocks[i]);

        // decrypt the message using the kth root function
        value = kRoot(k, blocks[i], p, q );

        // grab the two character values from the decrypted block
        charB = (value % 100);
        charA = (value - charB) / 100;

        // decode each character from our unique character scheme into ASCII
        charA = decode(charA);
        charB = decode(charB);

        plainText.push(charA);
        plainText.push(charB);

        output_msg += charA + charB;
    }

    $("#msg_output").html( output_msg );
}

$(document).ready( function() {

    $("#encrypt").click( function() {

        var pq = $("#PQ").val(), k = $("#K").val(), message=$("#msg_input").val();
        var p = $("#P").val(), q = $("#Q").val();

        // this tries to use the PQ textbox, but if nothing is there, multiply P and Q together as a fallback
        if( pq.length == 0 ) { pq = parseInt(p, 10) * parseInt(q, 10); }
        else { pq = parseInt(pq, 10); }

        k  = parseInt(k, 10);

        encrypt(message, pq, k);
    });

    $("#decrypt").click( function() {

        var p = $("#P").val(), q = $("#Q").val(), k = $("#K").val(), message=$("#msg_input").val();

        p  = parseInt(p, 10);
        q  = parseInt(q, 10);
        k  = parseInt(k, 10);

        decrypt( message, p, q, k );
    } );

    $("#crackPQ").click( function() {
        // this code tries to break an encrypted message using a public key pair: pq and k


    } );
});